# Create deploy folder
mkdir public

# Copy page to ./public
mv ./scripts/ ./public/scripts/
mv ./styles/ ./public/styles/
mv ./index.html ./public/index.html